#include "AccelerometerReader.h"

AccelerometerReader::AccelerometerReader()
  : clashThreshold(40)
  , clashDuration(1)
  , swingThreshold(5)
  , swingDuration(30)
  , clashStatus(false)
  , swingStatus(false)
{
}

void AccelerometerReader::setup()
{
  Wire.begin();
  mpu6050.initialize();  // initialize device

  mpu6050.setIntZeroMotionEnabled(true);
  mpu6050.setIntMotionEnabled(true);

//  accelgyro.setDHPFMode(1);
//  accelgyro.setDLPFMode(1);

  mpu6050.setMotionDetectionThreshold(clashThreshold);
  mpu6050.setMotionDetectionDuration(clashDuration);

  mpu6050.setZeroMotionDetectionThreshold(swingThreshold);
  mpu6050.setZeroMotionDetectionDuration(swingDuration);
}

AccelerometerReader::~AccelerometerReader()
{}


MoveType AccelerometerReader::getMove()
{
  clashStatus = mpu6050.getIntMotionStatus();
  swingStatus = mpu6050.getIntZeroMotionStatus();

  if (swingStatus)
    return MoveType::SWING;
  else if (clashStatus)
    return MoveType::CLASH;
  else
    return MoveType::IDLE;
}
