#pragma once

#include "MPU6050.h"
#include "MoveType.h"

class AccelerometerReader
{
public:
  AccelerometerReader();
  ~AccelerometerReader();
  void setup();

  MoveType getMove();

private:
  uint8_t clashThreshold;
  uint8_t clashDuration;

  uint8_t swingThreshold;
  uint8_t swingDuration;

  MPU6050 mpu6050;

  bool clashStatus;
  bool swingStatus;
};
